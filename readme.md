def create_grid(size):
    return [[' ' for _ in range(size)] for _ in range(size)]
