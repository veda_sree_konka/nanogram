def create_grid(size):
    return [[' ' for _ in range(size)] for _ in range(size)]

def solve_nanogram(puzzle, rows, columns):
    size = len(puzzle)
    
def is_valid_row(row, constraints):
    row_string = ''.join(row)
    for constraint in constraints:
        if not is_valid_constraint(row_string, constraint):
            return False
    return True
    
def is_valid_constraint(row_string, constraint):
    segments = row_string.split(' ')
    for segment in segments:
        if len(segment) > 0 and len(segment) < constraint:
            return False
    return True
    
def is_valid_column(column, constraints):
    column_string = ''.join(column)
    for constraint in constraints:
        if not is_valid_constraint(column_string, constraint):
            return False
    return True
    
def solve(puzzle, rows, columns):
    if not any(' ' in row for row in puzzle):
        return puzzle
    for i in range(size):
        for j in range(size):
            if puzzle[i][j] == ' ':
                puzzle[i][j] = '#'
                if (is_valid_row(puzzle[i], rows[i]) and is_valid_column([row[j] for row in puzzle], columns[j])):
                    result = solve(puzzle, rows, columns)
                    if result:
                        return result
                    puzzle[i][j] = ' '
        return None
    return solve(puzzle, rows, columns)
 
size = 5
puzzle = create_grid(size)
rows = [[2], [1, 1], [2], [1], [3]]
columns = [[1], [2], [2], [1, 1], [2]]
solution = solve_nanogram(puzzle, rows, columns)
