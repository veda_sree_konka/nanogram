==============
Nonograms Guide
==============

Introduction
------------

Nonograms, also known as Picross or Griddlers, are picture logic puzzles in which cells in a grid must be filled or left blank according to the numbers at the side of the grid. When solved correctly, the filled cells form a hidden picture.

Rules
-----

The rules of nonograms are simple:

1. You are given a rectangular grid with numbers placed at the top and left of the grid.
2. Each number represents a consecutive group of filled cells in that row or column.
3. The numbers are listed in the order they appear in the row or column, with at least one space between each group of filled cells.
4. Empty cells must be left blank.
5. The order of the numbers does not necessarily correspond to the order of the groups on the grid

Team Members
------------

The nonogram project was developed by the following team members:

- Vedasree
- Bhavani
- Bhavya Sri
